import React, { useState } from 'react'
import useFetch from './hooks/useFetch'
import Navbar from './components/Navbar'
import Home from './pages/Home'
import Favs from './pages/Favs'
import LastSearch from './pages/LastSearch'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom"

const App = () => {

    //get listfavs of local storage
    const listfavs = JSON.parse(localStorage.getItem('favs'))
    //States
    const [ search, setSearch ] = useState("happy")
    const [ limit, setLimit ] = useState(10)
    const [ favs, setFavs ] = useState(listfavs || [])
    //save favs in local storage
    localStorage.setItem('favs', JSON.stringify(favs))
    //vars
    const api_key = process.env.REACT_APP_API_KEY
    const url_api = `https://api.giphy.com/v1/gifs/search?api_key=${api_key}&q=dogs ${search}&limit=${limit}`
    const { loading, data } = useFetch(url_api)
 
    if(loading) return <h1>Cargando...</h1>

    const listGifs = data.map((e) => {
        return {
            id: e.id,
            title: e.title,
            url: e.images.downsized_large.url,
            fav: false
        }
    })
    
    return (
        <Router>
            <Navbar 
                setSearch={ setSearch } 
                setLimit={ setLimit }
            />
            <Switch>
                <Route path="/favs">
                    <Favs />
                </Route>
                <Route path="/last-search">
                    <LastSearch />
                </Route>
                <Route path="/" exact>
                    <Home
                        listGifs={ listGifs }
                        search={ search } 
                        limit={ limit }
                        setLimit={ setLimit }  
                        setFavs={ setFavs } 
                    />
                </Route>
            </Switch>
        </Router>
    )
}

export default App
