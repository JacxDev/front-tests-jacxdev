import React from 'react'
import Card from '../components/Card'


const BloqCards = ({search, limit, setLimit, setFavs, listGifs}) => {

    const handleClick = () => {
        setLimit(limit + 10)
    }

    return (
        <>
            <div className='container my-3 mx-auto px-5 grid grid-cols-1 sm:grid-cols-3 md:grid-cols-5 w-full gap-3'>
                {
                    listGifs.map((e) => {
                        return(
                            <Card 
                                key={ e.id }
                                title={ e.title }
                                url={ e.url }
                                id={ e.id }
                                setFavs={ setFavs }
                                fav={ e.fav }
                            />
                        )
                    })
                }
            </div>
            <div className="w-full flex justify-center">
                <button 
                    className="bg-purple-800 hover:bg-purple-600 text-white font-bold py-2 px-4 rounded my-5"
                    onClick={ handleClick }
                >
                    Search more...
                </button>
            </div>
        </>
    )
}
export default BloqCards
