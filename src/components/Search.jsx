import React, { useState } from 'react'

const Search = ({ setSearch, setLimit }) => {

    const lastsSearches = JSON.parse(localStorage.getItem('searchs'))

    const [ value, setValue ] = useState("")
    const [ lastSearch, setLastSearch ] = useState(lastsSearches || [])

    localStorage.setItem('searchs', JSON.stringify(lastSearch))

    const handleChange = (e) => {
        setValue(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        if(value.trim().length > 2){
            setSearch(value)
            setValue("")
            setLimit(10)
            setLastSearch([value, ...lastSearch])
        }
    }

    return (
        <>
            <form 
                onSubmit={ handleSubmit } 
                className="w-4/5"
            >
                <input 
                    type="text" 
                    value={ value }
                    onChange={ handleChange }
                    placeholder="Search dogs..."
                    className="w-full rounded h-5/6 text-center p-1"
                />
            </form>
        </>
    )
}

export default Search
