import React from 'react'
import './card.css'

const Card = ({ title, url, id, setFavs, fav }) => {
    const listFavs = JSON.parse(localStorage.getItem('favs'))

    const handleClick = () => {

        let filterFavs = listFavs.find( element => element['id'] === id )
        
        if(!filterFavs){
            setFavs((favs) => [...favs, {
                fav: true,
                id, 
                title,
                url,
            }])
        }
    }
    return (
        <div className="max-w-md bg-white rounded-xl overflow-hidden shadow-xl hover:shadow-1xl hover:scale-105 transform transition-all duration-500">
            <div className="p-4">
                <img className="rounded-xl max-h-56 w-full" src={ url } alt={ title } />
            </div>
            <div className="flex justify-between p-6">
                <div className="flex items-center space-x-4">
                    <h1 className="text-lg text-gray-900 font-bold line-clamp-1 title">
                        { title }
                    </h1>
                </div>
                <div className="flex space-x-6 items-center">
                    <div className="flex space-x-2 items-center pr-4 cursor-pointer" onClick={ handleClick }>
                        <span>
                            <i style={{  color:  fav ? 'yellow' : 'grey' }} className="fas fa-star"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Card
