import React from 'react'
import { Link } from 'react-router-dom'
import Search from './Search'

const Navbar = ({ setSearch, setLimit }) => {
    return (
        <div className="w-full h-16 bg-gray-800 m-0 flex justify-between items-center sticky top-0 z-50">
            <div className="flex items-center w-4/12">
                <Link to="/" >
                    <button className="md:w-20 rounded hover:bg-purple-800 mx-5">
                        <span><i style={{ color: '#FFF' }} className="fas fa-home"></i></span>
                        <p className="text-white hidden md:block">Home</p>
                    </button>
                </Link>
                <Link to="/favs" >
                    <button className="md:w-20 rounded hover:bg-purple-800 mx-5">
                        <span><i style={{ color: 'yellow' }} className="fas fa-star"></i></span>
                        <p className="text-white hidden md:block">My Favs</p>
                    </button>
                </Link>
            </div>
            <div className="flex justify-around items-center w-8/12">
                <Search 
                    setSearch={ setSearch } 
                    setLimit={ setLimit }    
                />
                 <Link to="/last-search" >
                    <button className="md:w-20 rounded hover:bg-purple-800 mx-5">
                        <span><i style={{ color: '#FFF' }} className="fas fa-history"></i></span>
                        <p className="text-white hidden md:block">History</p>
                    </button>
                </Link>
            </div>
        </div>
    )
}

export default Navbar
