import React from 'react'
import BloqCards from '../Bloqs/BloqCards'

const Home = ({ search, limit, setLimit, setFavs, listGifs }) => {

    return (
        <div className="w-full">
            <BloqCards  
                search={ search } 
                limit={ limit }
                setLimit={ setLimit }  
                setFavs={ setFavs } 
                listGifs={listGifs}
            /> 
        </div>
    )
}

export default Home
