import React from 'react'
import Card from '../components/Card'

const Favs = () => {

    const listfavs = JSON.parse(localStorage.getItem('favs'))

    if(listfavs.length === 0 ) return <h1>No tienes Favoritos</h1>

    return (
        <div className='container my-3 mx-auto px-5 grid grid-cols-1 sm:grid-cols-3 md:grid-cols-5 w-full gap-3'>
            {
                listfavs.map((e) => {
                    return(
                        <Card 
                            key={ e.id }
                            url={ e.url }
                            title={ e.title }
                            id={ e.id }
                        />
                    )
                })
            }
        </div>
    )
    
}

export default Favs
