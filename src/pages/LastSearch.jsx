import React from 'react'

const LastSearch = () => {
    const lastsSearches = JSON.parse(localStorage.getItem('searchs'))

    return (
        <div>
            <table class="rounded-t-lg m-5 w-5/6 mx-auto bg-gray-200 text-gray-800">
                <tr class="text-left border-b-2 border-gray-300">
                    <th class="px-4 py-3">Last searches</th>
                </tr>
                {
                    lastsSearches.map(e => (
                        <tr class="bg-gray-100 border-b border-gray-200">
                            <td class="px-4 py-3"> { e } </td>
                        </tr> 
                    )) 
                }
            </table>
        </div>
    )
}

export default LastSearch
