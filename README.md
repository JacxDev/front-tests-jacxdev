
### `git clone https://github.com/JacxDev/worky-test.git`
Url repo: [https://github.com/JacxDev/worky-test.git](https://github.com/JacxDev/worky-test.git)

### `cd worky-test`
### `npm run install`

Url demo: [https://worky-test.vercel.app/](https://worky-test.vercel.app/)

create .env file with enviroment REACT_APP_API_KEY 
Open [https://developers.giphy.com/dashboard/](https://developers.giphy.com/dashboard/) and create new API key
REACT_APP_API_KEY=your_giphy_api_key

### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

